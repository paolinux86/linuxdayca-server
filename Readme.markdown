# LinuxDay Cagliari Admin

Questo progetto consiste in API REST e WebApp di amministrazione del LinuxDay organizzato a Cagliari dal GULCh.
Il progetto è sviluppato con [**Django** 1.7rc2](https://www.djangoproject.com/), insieme al framework [**Django REST Framework**](http://www.django-rest-framework.org/)

## Dipendenze

Sono richiesti i seguenti software:

* PostgreSQL 9 o superiore
* Python 2.7 o superiore
* Pip 1.5 o superiore
* Node.js 1.4 o superiore

## Setup

1. Installare le dipendenze Python (da root)

        pip install -r requirements.txt

2. Installare le dipendenze per Node.js (da root)

        npm install -g grunt-cli bower less

3. Installare le dipendenze del progetto (come utente normale)

        npm install ; bower install

4. Compilare il progetto

        grunt compile   # Compila il progetto ma non esegue la minificazione dei javascript
        grunt build     # Come compile ma con i sorgenti javascript minificati

5. Setup del database

        psql < etc/000_schema.sql
        python manage.py syncdb

6. Avviare il server

        python manage.py runserver 0.0.0.0:8000

# Licenza

Questo progetto viene rilasciato con licenza GPLv2.
Il testo completo della licenza è contenuto nel file *LICENSE*.

    LinuxDay Cagliari Admin
    Copyright (C) 2014  Paolo Cortis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
