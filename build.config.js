/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  build_dir: 'linuxdayca/local-assets',
  compile_dir: 'bin',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    js: [ 'linuxdayca/src/**/*.js', '!linuxdayca/src/**/*.spec.js', '!linuxdayca/src/assets/**/*.js' ],
    annotatedjs: [ 'linuxdayca/local-assets/linuxdayca/**/*.js' ],
	ngi18njs: [ 'linuxdayca/assets/angular-i18n/*.js' ],
    jsunit: [ 'linuxdayca/src/**/*.spec.js' ],

    html_dir: 'linuxdayca/src/views',

    atpl: [ 'linuxdayca/src/app/**/*.tpl.html' ],
    ctpl: [ 'linuxdayca/src/common/**/*.tpl.html' ],

    index: [ 'linuxdayca/src/index.html' ],
    less: 'linuxdayca/src/style.less'
  },

  /**
   * This is a collection of files used during testing only.
   */
  test_files: {
    js: [
      'vendor/angular-mocks/angular-mocks.js'
    ]
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
		'linuxdayca/assets/jquery/dist/jquery.min.js',
		'linuxdayca/assets/angular/angular.min.js',
		'linuxdayca/assets/bootstrap/dist/js/bootstrap.min.js',
		'linuxdayca/assets/angular-resource/angular-resource.min.js',
		'linuxdayca/assets/angular-sanitize/angular-sanitize.min.js',
		'linuxdayca/assets/angular-route/angular-route.min.js',
		'linuxdayca/assets/angular-cookies/angular-cookies.min.js',
		'linuxdayca/assets/angular-translate/angular-translate.min.js',
		'linuxdayca/assets/angular-translate-loader-partial/angular-translate-loader-partial.min.js',
		'linuxdayca/assets/less/dist/less-1.7.4.min.js',
		'linuxdayca/assets/angular-ui-bootstrap-bower/ui-bootstrap-tpls.min.js',
		'linuxdayca/assets/select2/select2.min.js',
		'linuxdayca/assets/angular-ui-select2/src/select2.js',
		'linuxdayca/assets/lodash/dist/lodash.underscore.min.js',
		'linuxdayca/assets/angular-google-maps/dist/angular-google-maps.min.js',
		'linuxdayca/assets/angular-ui-router/release/angular-ui-router.min.js'
    ],
    css: [
// 		'linuxdayca/assets/bootstrap/dist/css/bootstrap.min.css',
		'linuxdayca/assets/bootswatch/spacelab/bootstrap.min.css',
		'linuxdayca/assets/normalize-css/normalize.css',
		'linuxdayca/assets/select2/select2.css'
    ],
    assets: [
		'linuxdayca/assets/bootstrap/fonts/*',
		'linuxdayca/assets/select2/select2-spinner.gif',
		'linuxdayca/assets/select2/select2.png'
    ]
  },
};
