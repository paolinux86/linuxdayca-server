DROP USER IF EXISTS linuxdayca;

CREATE ROLE linuxdayca LOGIN
	ENCRYPTED PASSWORD 'md577c56cc05e486d4e11d084f63e9ac28b'
	NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;

DROP DATABASE IF EXISTS linuxdayca;

CREATE DATABASE linuxdayca
	WITH OWNER = linuxdayca
	ENCODING = 'UTF8'
	TABLESPACE = pg_default
	LC_COLLATE = 'C'
	LC_CTYPE = 'it_IT.utf8'
	CONNECTION LIMIT = -1;

\connect linuxdayca;

CREATE SEQUENCE address_seq START 0 MINVALUE 0;
CREATE TABLE address (
	address_id BIGINT DEFAULT nextval('address_seq') PRIMARY KEY,
	address VARCHAR(255) NOT NULL,
	city VARCHAR(80) NOT NULL,
	state VARCHAR(80) NOT NULL,
	latitude VARCHAR(15),
	longitude VARCHAR(15),
	map BYTEA
);
ALTER TABLE address_seq OWNER TO linuxdayca;
ALTER TABLE address OWNER TO linuxdayca;

CREATE SEQUENCE conference_seq START 0 MINVALUE 0;
CREATE TABLE conference (
	conference_id BIGINT DEFAULT nextval('conference_seq') PRIMARY KEY,
	title VARCHAR(255) NOT NULL,
	subtitle VARCHAR(1024),
	venue BIGINT NOT NULL REFERENCES address(address_id),
	start_date DATE NOT NULL,
	end_date DATE NOT NULL
);
ALTER TABLE conference_seq OWNER TO linuxdayca;
ALTER TABLE conference OWNER TO linuxdayca;

CREATE SEQUENCE day_seq START 0 MINVALUE 0;
CREATE TABLE day (
	day_id BIGINT DEFAULT nextval('day_seq') PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
	day_date DATE NOT NULL,
	conference BIGINT NOT NULL REFERENCES conference(conference_id)
);
ALTER TABLE day_seq OWNER TO linuxdayca;
ALTER TABLE day OWNER TO linuxdayca;

CREATE TABLE room (
	name VARCHAR(100) PRIMARY KEY
);
ALTER TABLE room OWNER TO linuxdayca;

CREATE SEQUENCE person_seq START 0 MINVALUE 0;
CREATE TABLE person (
	person_id BIGINT DEFAULT nextval('person_seq') PRIMARY KEY,
	name VARCHAR(80) NOT NULL,
	middle_name VARCHAR(80),
	surname VARCHAR(80) NOT NULL,
	description VARCHAR(2048) NOT NULL,
	photo BYTEA
);
ALTER TABLE person_seq OWNER TO linuxdayca;
ALTER TABLE person OWNER TO linuxdayca;

CREATE SEQUENCE track_seq START 0 MINVALUE 0;
CREATE TABLE track (
	track_id BIGINT DEFAULT nextval('track_seq') PRIMARY KEY,
	title VARCHAR(255) NOT NULL,
	subtitle VARCHAR(1024),
	room VARCHAR(100) NOT NULL REFERENCES room(name),
	day BIGINT NOT NULL REFERENCES day(day_id)
);
ALTER TABLE track_seq OWNER TO linuxdayca;
ALTER TABLE track OWNER TO linuxdayca;

CREATE TABLE event_type (
	code VARCHAR(20) PRIMARY KEY,
	description VARCHAR(1024),
	image BYTEA
);
ALTER TABLE event_type OWNER TO linuxdayca;

CREATE SEQUENCE event_seq START 0 MINVALUE 0;
CREATE TABLE event (
	event_id BIGINT DEFAULT nextval('event_seq') PRIMARY KEY,
	start_date TIMESTAMP WITH TIME ZONE NOT NULL,
	duration integer,
	title VARCHAR(200) NOT NULL,
	subtitle VARCHAR(512),
	abstract VARCHAR(2048),
	description VARCHAR(1024),
	track BIGINT NOT NULL REFERENCES track(track_id),
	event_type VARCHAR(20) NOT NULL REFERENCES event_type(code)
);
ALTER TABLE event_seq OWNER TO linuxdayca;
ALTER TABLE event OWNER TO linuxdayca;

CREATE SEQUENCE links_seq START 0 MINVALUE 0;
CREATE TABLE links (
	link_id BIGINT DEFAULT nextval('links_seq') PRIMARY KEY,
	link VARCHAR(1024) NOT NULL,
	description VARCHAR(200) NOT NULL,
	event BIGINT NOT NULL REFERENCES event(event_id)
);
ALTER TABLE links_seq OWNER TO linuxdayca;
ALTER TABLE links OWNER TO linuxdayca;

CREATE TABLE person_presents_event (
	person BIGINT NOT NULL REFERENCES person(person_id),
	event BIGINT NOT NULL REFERENCES event(event_id),
	PRIMARY KEY(person, event)
);
ALTER TABLE person_presents_event OWNER TO linuxdayca;
