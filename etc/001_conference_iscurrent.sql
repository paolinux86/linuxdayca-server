ALTER TABLE conference ADD COLUMN is_current BOOLEAN DEFAULT FALSE NOT NULL;

UPDATE conference SET is_current = FALSE;

-- ############################################

CREATE OR REPLACE FUNCTION ensure_one_current_conference() RETURNS TRIGGER AS $$
	BEGIN
		IF (TG_OP = 'UPDATE') THEN
			IF (NEW.is_current = TRUE) THEN
				UPDATE conference SET is_current = FALSE WHERE conference_id <> NEW.conference_id;
			END IF;
		ELSIF (TG_OP = 'INSERT') THEN
			IF (NEW.is_current = TRUE) THEN
				UPDATE conference SET is_current = FALSE;
			END IF;
		END IF;

		RETURN NEW;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_ensure_one_current_conference
	BEFORE INSERT OR UPDATE ON conference
	FOR EACH ROW EXECUTE PROCEDURE ensure_one_current_conference();

-- ############################################

CREATE OR REPLACE FUNCTION lock_archived_conferences() RETURNS TRIGGER AS $$
	BEGIN
		IF (OLD.is_current = TRUE) THEN
			RETURN OLD;
		END IF;

		RETURN NEW;
	END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg_lock_archived_conferences
	BEFORE UPDATE ON conference
	FOR EACH ROW EXECUTE PROCEDURE lock_archived_conferences();
