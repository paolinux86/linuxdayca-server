from .models import *

from django.contrib import admin

class EventAdmin(admin.ModelAdmin):
	class PeopleInline(admin.TabularInline):
		model = Event.person.through

	class LinksInline(admin.StackedInline):
		model = Links
		pk_name = 'event'
		fields = ['link', 'description']

	fields = [ "start_date", "duration", "title", "subtitle", "abstract", "description", "track", "event_type" ]
	list_display = [ "start_date", "duration", "title", "track", "event_type" ]
	inlines = [ LinksInline, PeopleInline ]

# Register your models here.
admin.site.register(Address, Address.Admin)
admin.site.register(Conference, Conference.Admin)
admin.site.register(Day, Day.Admin)
admin.site.register(Event, EventAdmin)
admin.site.register(EventType, EventType.Admin)
admin.site.register(Links, Links.Admin)
admin.site.register(Person, Person.Admin)
admin.site.register(PersonPresentsEvent, PersonPresentsEvent.Admin)
admin.site.register(Room)
admin.site.register(Track, Track.Admin)
