# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404

from django.contrib.auth.models import User, AnonymousUser
from django.http import Http404
from django.core.exceptions import PermissionDenied

from datetime import datetime, timedelta

from rest_framework import generics, permissions

from .serializers import *
from .appserializers import *
from .models import *

def daterange(start_date, end_date):
	for n in range(int ((end_date - start_date).days) + 1):
		yield start_date + timedelta(n)

def determineWeekDayName(date):
	dayNames = [
		u"Domenica",
		u"Lunedì",
		u"Martedì",
		u"Mercoledì",
		u"Giovedì",
		u"Venerdì",
		u"Sabato"
	]

	return dayNames[date.weekday()]

def currentConference():
	return 22

class CurrentConferenceDetail(generics.RetrieveAPIView):
	"""
	Returns the current Conference
	"""
	serializer_class = ConferenceWithDaysSerializer
	permission_classes = [
		permissions.AllowAny
	]
	def get_object(self):
		return get_object_or_404(Conference, is_current=True)

class ConferenceDetail(generics.RetrieveUpdateDestroyAPIView):
	"""
	Returns the Conference detail (GET), updates it (PUT, PATCH) or deletes it
	"""
	serializer_class = ConferenceWithDaysSerializer
	permission_classes = [
		permissions.AllowAny
	]
	def get_object(self):
		return get_object_or_404(Conference, conference_id=self.kwargs.get("pk"))

class CreateConference(generics.CreateAPIView):
	"""
	Creates a new Conference
	"""
	serializer_class = ConferenceWithDaysSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def post_save(self, obj, created=False):
		if created == False:
			return

		for single_date in daterange(obj.start_date, obj.end_date):
			name = determineWeekDayName(single_date)
			day = Day.objects.create(day_date=single_date, name=name, conference=obj)
			#obj.days.append(day.pk)

class RetrieveConferenceStats(generics.RetrieveAPIView):
	"""
	Returns some statistics for given Conference id
	"""
	model = ConferenceStatistics
	serializer_class = ConferenceStatisticsSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_object(self):
		conference_id = self.kwargs.get("pk")
		get_object_or_404(Conference, conference_id=conference_id)

		events = Event.objects.filter(track__day__conference=conference_id)
		people = []

		for event in events:
			people.extend(event.person.all())

		return {
			"days": len(Day.objects.filter(conference=conference_id)),
			"tracks": len(Track.objects.filter(day__conference=conference_id)),
			"events": len(events),
			"people": len(people),
		}

class ListDaysByConference(generics.ListAPIView):
	"""
	Returns the list of Days by Conference ID
	"""
	model = Day
	serializer_class = DayWithTracksSerializer
	permission_classes = [
		permissions.AllowAny
	]
	lookup_field = "conference"

	def get_queryset(self):
		queryset = super(ListDaysByConference, self).get_queryset()
		return queryset.filter(conference__conference_id = self.kwargs.get("conference"))

class ListDaysByCurrentConference(generics.ListAPIView):
	"""
	Returns the list of Days of current Conference
	"""
	model = Day
	serializer_class = DayWithTracksSerializer
	permission_classes = [
		permissions.AllowAny
	]
	lookup_field = "conference"

	def get_queryset(self):
		queryset = super(ListDaysByCurrentConference, self).get_queryset()
		return queryset.filter(conference__is_current=True)

class DayDetail(generics.RetrieveAPIView):
	"""
	Returns the detail of specified Day
	"""
	model = Day
	serializer_class = DayWithTracksSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_queryset(self):
		queryset = super(DayDetail, self).get_queryset()
		return queryset.filter(day_id = self.kwargs.get("pk"))

class ListTracksByDay(generics.ListAPIView):
	"""
	Returns the list of Tracks by Day ID
	"""
	model = Track
	serializer_class = TrackWithEventsSerializer
	permission_classes = [
		permissions.AllowAny
	]
	lookup_field = "day"

	def get_queryset(self):
		queryset = super(ListTracksByDay, self).get_queryset()
		return queryset.filter(day__day_id = self.kwargs.get("day"))

class TrackDetail(generics.RetrieveAPIView):
	"""
	Returns the detail of specified Track
	"""
	model = Track
	serializer_class = TrackWithEventsSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_queryset(self):
		queryset = super(TrackDetail, self).get_queryset()
		return queryset.filter(track_id = self.kwargs.get("pk"))

class EventDetail(generics.RetrieveAPIView):
	"""
	Returns the detail of specified Event
	"""
	model = Event
	serializer_class = EventSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_queryset(self):
		queryset = super(EventDetail, self).get_queryset()
		return queryset.filter(event_id = self.kwargs.get("pk"))

class PersonDetail(generics.RetrieveAPIView):
	"""
	Returns the detail of specified Person
	"""
	model = Person
	serializer_class = PersonSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_queryset(self):
		queryset = super(PersonDetail, self).get_queryset()
		return queryset.filter(person_id = self.kwargs.get("pk"))

class UserDetail(generics.RetrieveAPIView):
	"""
	Returns the detail of specified User
	"""
	model = User
	serializer_class = UserSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_object(self):
		if isinstance(self.request.user, AnonymousUser):
			raise PermissionDenied()

		return self.request.user

class ListAddresses(generics.ListAPIView):
	"""
	Returns the list of addresses
	"""
	model = Address
	serializer_class = AddressSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

class ListRooms(generics.ListAPIView):
	"""
	Returns the list of rooms
	"""
	model = Room
	serializer_class = RoomSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

class ListEventTypes(generics.ListAPIView):
	"""
	Returns the list of event types
	"""
	model = EventType
	serializer_class = EventTypeSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

class ListPerson(generics.ListAPIView):
	"""
	Returns the list of people
	"""
	model = Person
	serializer_class = PersonSerializer
	permission_classes = [
		permissions.IsAuthenticated
	]

	def get_queryset(self):
		queryset = super(ListPerson, self).get_queryset()
		return queryset.order_by("surname", "name")


class AppConferenceDetail(generics.RetrieveAPIView):
	"""
	Returns the current Conference
	"""
	serializer_class = AppConferenceSerializer
	permission_classes = [
		permissions.AllowAny
	]
	def get_object(self):
		return get_object_or_404(Conference, is_current=True)

class PreviousConferencesList(generics.ListAPIView):
	"""
	Returns the list of previous conferences
	"""
	model = Conference
	serializer_class = ConferenceSerializer
	permission_classes = [
		permissions.AllowAny
	]

	def get_queryset(self):
		queryset = super(PreviousConferencesList, self).get_queryset()
		return queryset.filter(is_current=False).order_by("-conference_id")
