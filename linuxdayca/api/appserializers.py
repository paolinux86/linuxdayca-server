from rest_framework import serializers

from .models import *

class AppPersonSerializer(serializers.ModelSerializer):
	class Meta:
		model = Person
		fields = ( "person_id", "name", "middle_name", "surname", "description" )

class AppLinksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Links
		fields = ( "link_id", "link", "description" )

class AppEventTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = EventType
		fields = ( "code", "description" )

class AppEventSerializer(serializers.ModelSerializer):
	links = AppLinksSerializer(many=True)
	person = AppPersonSerializer(many=True)
	event_type = AppEventTypeSerializer(many=False)

	class Meta:
		model = Event
		fields = ( "event_id", "start_date", "duration", "title", "subtitle", "abstract", "description", "event_type", "person", "links" )

class AppRoomSerializer(serializers.ModelSerializer):
	class Meta:
		model = Room
		fields = ( "name", )

class AppTrackSerializer(serializers.ModelSerializer):
	events = AppEventSerializer(many=True)
	room = AppRoomSerializer(many=False)

	class Meta:
		model = Track
		fields = ( "track_id", "title", "subtitle", "room", "events" )

class AppDaySerializer(serializers.ModelSerializer):
	tracks = AppTrackSerializer(many=True)

	class Meta:
		model = Day
		fields = ( "day_id", "name", "day_date", "tracks" )

class AppAddressSerializer(serializers.ModelSerializer):
	class Meta:
		model = Address
		fields = ( "address_id", "address", "city", "state", "latitude", "longitude" )

class AppConferenceSerializer(serializers.ModelSerializer):
	days = AppDaySerializer(many=True)
	venue = AppAddressSerializer(many=False)

	class Meta:
		model = Conference
		fields = ( "conference_id", "title", "subtitle", "venue", "start_date", "end_date", "days" )
		read_only_fields = ( "conference_id", )
