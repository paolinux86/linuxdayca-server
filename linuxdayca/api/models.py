# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

def update_id(sequence_name, id_name):
	def _method_wrapper(func):
		'''A decorator for pulling a data object's ID value out of a
		user-defined sequence.  This gets around a limitation in
		django whereby we cannot supply our own sequence names.'''

		def decorated_function(self, force_insert=False, force_update=False, using=None, update_fields=None):
			# Grab a reference to the data object we want to update.
			data_object = self

			# Only update the ID if there isn't one yet.
			if getattr(data_object, id_name) is None:
				# Query the database for the next sequence value.
				from django.db import connection
				cursor = connection.cursor()
				cursor.execute("SELECT nextval(%s)", [sequence_name])
				row = cursor.fetchone()

				# Update the data object's ID with the returned sequence value.
				setattr(data_object, id_name, row[0])

			# Execute the function we're decorating.
			return func(self, force_insert, force_update, using, update_fields)

		return decorated_function

	return _method_wrapper

class Address(models.Model):
	address_id = models.BigIntegerField(primary_key=True)
	address = models.CharField(max_length=255)
	city = models.CharField(max_length=80)
	state = models.CharField(max_length=80)
	latitude = models.CharField(max_length=15, blank=True)
	longitude = models.CharField(max_length=15, blank=True)
	address_map = models.BinaryField(db_column = "map", blank=True, null=True)

	def __unicode__(self):
		return "%s - %s (%s)" % (self.address, self.city, self.state)

	#@update_id("address_seq", "address_id")
	#def save(self):
		## Now actually save the object.
		#super(Address, self).save()

	class Meta:
		managed = False
		db_table = "address"
		verbose_name = _("Address")
		verbose_name_plural = _("Addresses")

	class Admin(admin.ModelAdmin):
		fields = [ "address", "city", "state", "latitude", "longitude" ]
		list_display = ("address", "city", "state", "latitude", "longitude" )

class Conference(models.Model):
	conference_id = models.BigIntegerField(primary_key=True)
	title = models.CharField(max_length=255)
	subtitle = models.CharField(max_length=1024, blank=True)
	venue = models.ForeignKey(Address, db_column="venue")
	start_date = models.DateField()
	end_date = models.DateField()
	is_current = models.BooleanField(default=True)

	def __unicode__(self):
		return "%s" % (self.title)

	@update_id("conference_seq", "conference_id")
	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		# Now actually save the object.
		super(Conference, self).save(force_insert, force_update, using, update_fields)

	class Meta:
		managed = False
		db_table = "conference"
		verbose_name = _("Conference")
		verbose_name_plural = _("Conferences")
		ordering = ("-conference_id", )

	class Admin(admin.ModelAdmin):
		fields = [ "title", "subtitle", "venue", "start_date", "end_date" ]
		list_display = ( "title", "venue", "start_date", "end_date" )

class Day(models.Model):
	day_id = models.BigIntegerField(primary_key=True)
	name = models.CharField(max_length=40)
	day_date = models.DateField()
	conference = models.ForeignKey(Conference, db_column="conference", related_name="days")

	def __unicode__(self):
		return "%s - %s" % (self.name, datetime.strftime(self.day_date, "%d-%m-%Y"))

	@update_id("day_seq", "day_id")
	def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
		# Now actually save the object.
		super(Day, self).save(force_insert, force_update, using, update_fields)

	class Meta:
		managed = False
		db_table = "day"
		verbose_name = _("Day")
		verbose_name_plural = _("Days")

	class Admin(admin.ModelAdmin):
		fields = [ "name", "day_date", "conference" ]
		list_display = ( "name", "day_date", "conference" )

class Person(models.Model):
	person_id = models.BigIntegerField(primary_key=True)
	name = models.CharField(max_length=80)
	middle_name = models.CharField(max_length=80, blank=True)
	surname = models.CharField(max_length=80)
	description = models.CharField(max_length=2048)
	photo = models.BinaryField(blank=True, null=True)

	def __unicode__(self):
		return "%s %s" % (self.name, self.surname)

	#@update_id("person_seq", "person_id")
	#def save(self):
		## Now actually save the object.
		#super(Person, self).save()

	class Meta:
		managed = False
		db_table = "person"
		verbose_name = _("Person")
		verbose_name_plural = _("People")

	class Admin(admin.ModelAdmin):
		fields = [ "name", "middle_name", "surname", "description" ]
		list_display = [ "name", "middle_name", "surname" ]

class Event(models.Model):
	event_id = models.BigIntegerField(primary_key=True)
	start_date = models.DateTimeField()
	duration = models.IntegerField(blank=True, null=True)
	title = models.CharField(max_length=200)
	subtitle = models.CharField(max_length=512, blank=True)
	abstract = models.CharField(max_length=2048, blank=True)
	description = models.CharField(max_length=1024, blank=True)
	track = models.ForeignKey("Track", db_column="track", related_name="events")
	event_type = models.ForeignKey("EventType", db_column="event_type")
	person = models.ManyToManyField(Person, through="PersonPresentsEvent", through_fields=('event', 'person'))

	def __unicode__(self):
		return "%s" % (self.title)

	#@update_id("event_seq", "event_id")
	#def save(self):
		## Now actually save the object.
		#super(Event, self).save()

	class Meta:
		managed = False
		db_table = "event"
		verbose_name = _("Event")
		verbose_name_plural = _("Events")

class EventType(models.Model):
	code = models.CharField(primary_key=True, max_length=20)
	description = models.CharField(max_length=1024, blank=True)
	image = models.BinaryField(blank=True, null=True)

	def __unicode__(self):
		return "%s" % (self.code)

	class Meta:
		managed = False
		db_table = "event_type"
		verbose_name = _("Event Type")
		verbose_name_plural = _("Event Types")

	class Admin(admin.ModelAdmin):
		fields = [ "code", "description" ]
		list_display = [ "code", "description" ]

class Links(models.Model):
	link_id = models.BigIntegerField(primary_key=True)
	link = models.CharField(max_length=1024)
	description = models.CharField(max_length=200)
	event = models.ForeignKey(Event, db_column="event", related_name="links")

	def __unicode__(self):
		return "%s (%s)" % (self.link, self.description)

	#@update_id("links_seq", "link_id")
	#def save(self):
		## Now actually save the object.
		#super(Links, self).save()

	class Meta:
		managed = False
		db_table = "links"
		verbose_name = _("Link")
		verbose_name_plural = _("Links")

	class Admin(admin.ModelAdmin):
		fields = [ "link", "description", "event" ]
		list_display = [ "link", "description", "event" ]

class PersonPresentsEvent(models.Model):
	person = models.ForeignKey(Person, db_column="person", primary_key=True)
	event = models.ForeignKey(Event, db_column="event")

	def __unicode__(self):
		return "%s presenta %s" % (self.person, self.event)

	class Meta:
		managed = False
		db_table = "person_presents_event"
		verbose_name = _("Person presents event")
		verbose_name_plural = _("Person/People presents event(s)")
		unique_together = (("person", "event"),)

	class Admin(admin.ModelAdmin):
		fields = [ "person", "event" ]
		list_display = [ "person", "event" ]

class Room(models.Model):
	name = models.CharField(primary_key=True, max_length=100)

	def __unicode__(self):
		return "%s" % (self.name)

	class Meta:
		managed = False
		db_table = "room"
		verbose_name = _("Room")
		verbose_name_plural = _("Rooms")

class Track(models.Model):
	track_id = models.BigIntegerField(primary_key=True)
	title = models.CharField(max_length=255)
	subtitle = models.CharField(max_length=1024, blank=True)
	room = models.ForeignKey(Room, db_column="room")
	day = models.ForeignKey(Day, db_column="day", related_name="tracks")

	def __unicode__(self):
		return "%s" % (self.title)

	#@update_id("track_seq", "track_id")
	#def save(self):
		## Now actually save the object.
		#super(Track, self).save()

	class Meta:
		managed = False
		db_table = "track"
		verbose_name = _("Track")
		verbose_name_plural = _("Tracks")

	class Admin(admin.ModelAdmin):
		fields = [ "title", "subtitle", "room", "day" ]
		list_display = [ "title", "subtitle", "room", "day" ]

# FAKE MODEL
class ConferenceStatistics(models.Model):
	days = models.IntegerField()
	tracks = models.IntegerField()
	events = models.IntegerField()
	people = models.IntegerField()

	class Meta:
		managed = False

