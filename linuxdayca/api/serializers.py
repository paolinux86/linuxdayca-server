from rest_framework import serializers

from django.contrib.auth.models import User

from .models import *

class ConferenceSerializer(serializers.ModelSerializer):
	start_date = serializers.DateField(format="U")

	class Meta:
		model = Conference
		fields = ( "conference_id", "title", "subtitle", "venue", "start_date", "end_date" )
		read_only_fields = ( "conference_id", )

class ConferenceWithDaysSerializer(serializers.ModelSerializer):
	class Meta:
		model = Conference
		fields = ( "conference_id", "title", "subtitle", "venue", "start_date", "end_date", "days" )
		read_only_fields = ( "conference_id", )

class DayWithTracksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Day
		fields = ( "day_id", "name", "day_date", "conference", "tracks" )

class DaySerializer(serializers.ModelSerializer):
	class Meta:
		model = Day
		fields = ( "day_id", "name", "day_date", "conference" )

class TrackSerializer(serializers.ModelSerializer):
	class Meta:
		model = Track
		fields = ( "track_id", "title", "subtitle", "room", "day" )

class TrackWithEventsSerializer(serializers.ModelSerializer):
	class Meta:
		model = Track
		fields = ( "track_id", "title", "subtitle", "room", "day", "events" )

class PersonSerializer(serializers.ModelSerializer):
	class Meta:
		model = Person
		fields = ( "name", "middle_name", "surname", "description" )

class LinksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Links
		fields = ( "link_id", "link", "description" )

class EventSerializer(serializers.ModelSerializer):
	links = LinksSerializer(many=True)

	class Meta:
		model = Event
		fields = ( "event_id", "start_date", "duration", "title", "subtitle", "abstract", "description", "track", "event_type", "person", "links" )

class PersonSerializer(serializers.ModelSerializer):
	class Meta:
		model = Person
		fields = ( "person_id", "name", "middle_name", "surname", "description" )

class PersonPhotoSerializer(serializers.ModelSerializer):
	class Meta:
		model = Person
		fields = ( "person_id", "name", "middle_name", "surname", "description" )

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ( "username", "first_name", "last_name" )

class AddressSerializer(serializers.ModelSerializer):
	class Meta:
		model = Address
		fields = ( "address_id", "address", "city", "state", "latitude", "longitude" )

class RoomSerializer(serializers.ModelSerializer):
	class Meta:
		model = Room
		fields = ( "name", )

class EventTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = EventType
		fields = ( "code", "description" )

class ConferenceStatisticsSerializer(serializers.ModelSerializer):
	class Meta:
		model = ConferenceStatistics
		fields = ( "days", "tracks", "events", "people" )
