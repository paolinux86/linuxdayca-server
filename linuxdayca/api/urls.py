from django.conf.urls import patterns, url, include

from .api import *
import views

conference_urls = patterns('',
	url(r'^/current$', CurrentConferenceDetail.as_view(), name='current-conference'),
	url(r'^/app', AppConferenceDetail.as_view(), name='app-current-conference'),
	url(r'^/previous', PreviousConferencesList.as_view(), name='previous-conferences'),
	url(r'^/new$', CreateConference.as_view(), name='create-conference'),
	url(r'^/(?P<pk>[0-9]+)/stats', RetrieveConferenceStats.as_view(), name='conference-stats'),
	url(r'^/(?P<pk>[0-9]+)/', ConferenceDetail.as_view(), name='conference-detail'),
)

day_urls = patterns('',
	url(r'^/by-conference/(?P<conference>[0-9]+)', ListDaysByConference.as_view(), name='list-days-by-conference'),
	url(r'^/by-conference/current', ListDaysByCurrentConference.as_view(), name='list-days-by-current-conference'),
	url(r'^/(?P<pk>[0-9]+)', DayDetail.as_view(), name='day-detail'),
)

track_urls = patterns('',
	url(r'^/by-day/(?P<day>[0-9]+)', ListTracksByDay.as_view(), name='list-tracks-by-day'),
	url(r'^/(?P<pk>[0-9]+)', TrackDetail.as_view(), name='track-detail'),
)

event_urls = patterns('',
	url(r'^/(?P<pk>[0-9]+)', EventDetail.as_view(), name='event-detail'),
)

event_type_urls = patterns('',
	url(r'^/$', ListEventTypes.as_view(), name='event-type-list'),
)

person_urls = patterns('',
	url(r'^/$', ListPerson.as_view(), name='person-list'),
	url(r'^/(?P<pk>[0-9]+)/photo', "linuxdayca.api.views.person_photo", name='person-photo'),
	url(r'^/(?P<pk>[0-9]+)', PersonDetail.as_view(), name='person-detail'),
)

user_urls = patterns('',
	url(r'^/$', UserDetail.as_view(), name='current-user-detail'),
)

address_urls = patterns('',
	url(r'^/$', ListAddresses.as_view(), name='address-list'),
)

room_urls = patterns('',
	url(r'^/$', ListRooms.as_view(), name='room-list'),
)

urlpatterns = patterns('',
    url(r'^conference', include(conference_urls)),
    url(r'^day', include(day_urls)),
    url(r'^track', include(track_urls)),
    url(r'^event', include(event_urls)),
    url(r'^event_type', include(event_type_urls)),
    url(r'^person', include(person_urls)),
    url(r'^user', include(user_urls)),
    url(r'^address', include(address_urls)),
    url(r'^room', include(room_urls)),
)
