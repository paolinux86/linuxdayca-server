from django.shortcuts import render, get_object_or_404
from django.http import Http404

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView

import json, base64

from .models import *

def login_view(request):
	username = request.POST['login_username']
	password = request.POST['login_password']
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
			login(request, user)
			return HttpResponseRedirect("/")
		else:
			return HttpResponseRedirect("/#/error/403")
	else:
		return HttpResponseRedirect("/?e=l")

def logout_view(request):
	logout(request)
	return HttpResponseRedirect("/")

def doDownload(request, blob):
	j = json.loads(blob)
	image = base64.standard_b64decode(j["content"])
	mime = j["mimeType"]
	filename = j["filename"]

	if mime is None or mime == "":
		mime = "application/octet-stream"

	response = HttpResponse()
	response["Content-Type"] = mime
	response["Content-Length"] = str(len(image))

	# To inspect details for the below code, see http://greenbytes.de/tech/tc2231/
	if u'WebKit' in request.META['HTTP_USER_AGENT']:
		# Safari 3.0 and Chrome 2.0 accepts UTF-8 encoded string directly.
		filename_header = 'filename=%s' % filename.encode('utf-8')
	elif u'MSIE' in request.META['HTTP_USER_AGENT']:
		# IE does not support internationalized filename at all.
		# It can only recognize internationalized URL, so we do the trick via routing rules.
		filename_header = ''
	else:
		# For others like Firefox, we follow RFC2231 (encoding extension in HTTP headers).
		filename_header = 'filename*=UTF-8\'\'%s' % urllib.quote(filename.encode('utf-8'))

	response["Content-Disposition"] = "attachment; " + filename_header
	response.write(image)

	return response

def person_photo(request, pk):
	person = get_object_or_404(Person, pk=pk)

	if person.photo is None:
		raise Http404

	blob = bytes(person.photo)
	return doDownload(request, blob)

class CustomTemplateView(TemplateView):
	def get_template_names(self):
		return self.kwargs["template_name"] + ".html"

#j = '{ "content": "iVBORw0KGgoAAAANSUhEUgAAAFAAAAAPBAMAAAH24GUqAAAABGdBTUEAALGPC/xhBQAAAC1QTFRF////+fn58fHx39/fzc3NvLy8qqqqmZmZh4eHeXl5ZGRkUlJSQUFBOTk5MzMzwfEwygAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfUDBsLFA+11+wjAAAAGnRFWHRDb21tZW50AENyZWF0ZWQgYnkgU3d8dENoYNJBjgAAAAC8SURBVHjajVHJDcIwEBwRPoBY8eObEuBHWZRCCVACDVBCSkAbIfggbQ3s4VyKJTxar0bjyWZsQwxoiVkQfMtCBG5IAmD0VHoAncpOdQAnmlRGVY8+6MTvDvqDKFsuPi8zp2SQF4G6TziIq/1hLi4/4SQrz6DiGmcKZSSSZGYOx/sTiVEEcmMjr/ncKdy4ueLmwWBRtRuxGyR9oagw4vRoJ0brukfh8st1I92rdzLGrK6n9xkmLo4oylh06h8yiVfEwqAoogAAAABJRU5ErkJggg==", "mimeType": "image/png", "filename": "img.png" }'

#person.photo = j
#person.save()

#return HttpResponseRedirect("/")
