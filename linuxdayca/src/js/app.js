"use strict";

var app = angular.module("ldcaApp",
	[
		"ngResource",
		"ngSanitize",
		"ngCookies",

		"ui.router",
		"ui.bootstrap",
		"ui.select2",

		"google-maps",
		"pascalprecht.translate",

		"ldca.date-module",

		"ldcaApp.services"
	]
);

var homeResolver = {
	data: function($q, $stateParams, $location, ConferenceService) {
		var id = $stateParams.id;
		var deferred = $q.defer();

		var objects = [];
		objects.push(ConferenceService.previousConferences.get().$promise);

		$q.all(objects)
		.then(function(response) {
			deferred.resolve(response);
		}, function(response) {
			var from = $location.$$url;
			if(response.status == 404) {
				$location.path("/error/421").search("from", from);
			} else if(response.status == 403) {
				$location.path("/error/403").search("from", from);
			} else {
				$location.path("/error/499").search("from", from);
			}
		});

		return deferred.promise;
	}
};

app.config(
	function($stateProvider, $urlRouterProvider, $interpolateProvider) {
// 		$interpolateProvider.startSymbol('{$');
// 		$interpolateProvider.endSymbol('$}');

		$urlRouterProvider.otherwise(function(o, $location) {
			return "/error/404?from=" + $location.$$url;
		});

		$stateProvider
		.state("home", {
			url: "/",
			templateUrl: "/static/views/home.html",
			controller: "HomeController",
			resolve: homeResolver
		})
		.state("home_alternate", {
			url: "",
			templateUrl: "/static/views/home.html",
			controller: "HomeController",
			resolve: homeResolver
		})
		.state("errors", {
			url: "/error/{error:4[0-9]{2}}?from",
			templateUrl: "/static/views/error.html",
			controller: "ErrorController"
		})
		.state("itemdetail", {
			url: "/{item:[A-Za-z]+}/show/{id:[0-9]+}",
			templateUrl: function($stateProvider) {
				var item = $stateProvider.item;

				return "/static/views/" + item + ".html";
			},
			controllerProvider: function($stateParams) {
				var ctrlName = $stateParams.item + "Controller";
				var firstLetterUpper = ctrlName.substr(0, 1).toUpperCase();

				return firstLetterUpper + ctrlName.substr(1);
			},
			resolve: {
				data: function($q, $stateParams, $location, ConferenceService, DayService, TrackService, EventService, PersonService) {
					var id = $stateParams.id;
					var deferred = $q.defer();

					var objects = [];
					var item = $stateParams.item;
					switch(item) {
						case "conference":
							objects.push(ConferenceService.conference.get({ id: id }).$promise);
							objects.push(ConferenceService.statistics.get({ id: id }).$promise);
							break;
						case "day":
							objects.push(DayService.day.get({ id: id }).$promise);
							break;
						case "track":
							objects.push(TrackService.track.get({ id: id }).$promise);
							break;
						case "event":
							objects.push(EventService.event.get({ id: id }).$promise);
							break;
						case "person":
							objects.push(PersonService.person.get({ id: id }).$promise);
							break;

						default:
							var from = $location.$$url;
							$location.path("/error/404").search("from", from);
							break;
					}

					$q.all(objects)
					.then(function(response) {
						deferred.resolve(response);
					}, function(response) {
						var from = $location.$$url;
						if(response.status == 404) {
							$location.path("/error/421").search("from", from);
						} else if(response.status == 403) {
							$location.path("/error/403").search("from", from);
						} else {
							$location.path("/error/499").search("from", from);
						}
					});

					return deferred.promise;
				}
			}
		})
		.state("itemlist", {
			url: "/{item:[A-Za-z]+}/list",
			templateUrl: function($stateProvider) {
				var item = $stateProvider.item;

				return "/static/views/" + item + "_list.html";
			},
			controllerProvider: function($stateParams) {
				var ctrlName = $stateParams.item + "ListController";
				var firstLetterUpper = ctrlName.substr(0, 1).toUpperCase();

				return firstLetterUpper + ctrlName.substr(1);
			},
			resolve: {
				data: function($q, $stateParams, $location, PersonService, EventTypeService, RoomService, AddressService) {
					var deferred = $q.defer();

					var objects = [];
					var item = $stateParams.item;
					switch(item) {
						case "person":
							objects.push(PersonService.person.get().$promise)
							break;
						case "eventType":
							objects.push(EventTypeService.event_type.get().$promise)
							break;
						case "room":
							objects.push(RoomService.room.get().$promise)
							break;
						case "address":
							objects.push(AddressService.address.get().$promise)
							break;

						default:
							var from = $location.$$url;
							$location.path("/error/404").search("from", from);
							break;
					}

					var deferred = $q.defer();
					$q
					.all(objects)
					.then(function(response) {
						deferred.resolve(response);
					}, function(response) {
						var from = $location.$$url;
						if(response.status == 404) {
							$location.path("/error/421").search("from", from);
						} else if(response.status == 403) {
							$location.path("/error/403").search("from", from);
						} else {
							$location.path("/error/499").search("from", from);
						}
					});

					return deferred.promise;
				}
			}
		})
		.state("conference-create", {
			url: "/conference/create",
			templateUrl: "/static/views/conference_create.html"
		})
		.state("track-create", {
			url: "/track/create",
			templateUrl: "/static/views/track_create.html"
		})
		.state("wizard", {
			url: "/wizard",
			templateUrl: "/static/views/wizard.html",
			controller: "WizardController"
		});
	}
)
.run(function($http, $cookies) {
	$http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
	// Add the following two lines
	$http.defaults.xsrfCookieName = 'csrftoken';
	$http.defaults.xsrfHeaderName = 'X-CSRFToken';
});
