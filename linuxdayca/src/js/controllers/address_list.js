"use strict";

angular.module("ldcaApp").controller("AddressListController",
	[
		"$scope",
		"$log",
		"$controller",
		"data",
		"AddressService",

		function($scope, $log, $controller, data, AddressService)
		{
			$controller("GenericListController", { $scope: $scope, $log: $log, data: data });
			$scope.onPagerChange = function() {
				AddressService.address.get({ page: $scope.currentPage }, $scope.setNewData);
			};
		}
	]
);
