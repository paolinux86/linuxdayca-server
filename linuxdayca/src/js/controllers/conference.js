"use strict";

angular.module("ldcaApp").controller("ConferenceController",
	[
		"$scope",
		"$log",
		"AddressService",
		"DayService",
		"data",

		function($scope, $log, AddressService, DayService, data)
		{
			$scope.currentConference = data[0];
			$scope.currentConference.start_date = new Date($scope.currentConference.start_date);
			$scope.currentConference.end_date = new Date($scope.currentConference.end_date);
			$scope.statistics = data[1];

			$scope.datepickers = {
				defaultOptions: {
					formatYear: "yyyy",
					startingDay: 0
				},
				isStartDateOpen: false,
				isEndDateOpen: false
			};

			$scope.editing = false;

			$scope.openStartDatePicker = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.datepickers.isStartDateOpen = !$scope.datepickers.isStartDateOpen;
			};

			$scope.openEndDatePicker = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.datepickers.isEndDateOpen = !$scope.datepickers.isEndDateOpen;
			};

			$scope.page = 1;
			$scope.venues = [];
			$scope.getVenues = function() {
				AddressService.address.get({ page: $scope.page++ }, function(data) {
					$scope.venues = $scope.venues.concat(data.results);
					if(data.next != null) {
						$scope.getVenues();
					} else {
						$scope.elaborateVenueIds();
					}
				});
			};
			$scope.getVenues();

			$scope.elaborateVenueIds = function() {
				$scope.venues_ids = $.map($scope.venues, function(val, i) {
					if($scope.conference.venue == val.address_id) {
						$scope.currentVenue = val;
						$scope.drawMarker();
					}

					return val.address_id;
				});
			};

			$scope.days = [];
			$.each($scope.currentConference.days, function(k, v) {
				$scope.days.push(DayService.day.get({id: v}));
			});

			$scope.map = {
				center: {
					latitude: 39.3,
					longitude: 9.05
				},
				zoom: 10,
			};

			$scope.drawMarker = function() {
				$scope.options = { scrollwheel: false };
				$scope.marker = {
					id: 0,
					coords: {
						latitude: $scope.currentVenue.latitude,
						longitude: $scope.currentVenue.longitude
					},
					options: { draggable: false }
				};
				$scope.map.center = {
					latitude: $scope.currentVenue.latitude,
					longitude: $scope.currentVenue.longitude
				};
				$scope.map.zoom = 17;
			};

			$scope.editConference = function() {
				$scope.editing = true;
			};
			$scope.disableEditConference = function() {
				$scope.editing = false;
			};

			// TODO: aggiornare mappa quando si cambia indirizzo
		}
	]
);
