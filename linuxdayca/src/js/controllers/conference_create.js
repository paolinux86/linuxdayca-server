"use strict";

angular.module("ldcaApp").controller("ConferenceCreateController",
	[
		"$scope",
		"$log",
		"$filter",
		"$modal",
		"AddressService",
		"ConferenceService",

		function($scope, $log, $filter, $modal, AddressService, ConferenceService)
		{
			$scope.forms = {};
			$scope.currentConference = {};

			$scope.datepickers = {
				defaultOptions: {
					formatYear: "yyyy",
					startingDay: 0
				},
				isStartDateOpen: false,
				isEndDateOpen: false
			};

			$scope.saveConference = function() {
				var conferenceToSave = $.extend({}, $scope.currentConference);
				var start_date = new Date($scope.currentConference.start_date);
				var end_date = new Date($scope.currentConference.end_date);
				conferenceToSave.start_date = $filter("date")(start_date, "yyyy-MM-dd");
				conferenceToSave.end_date = $filter("date")(end_date, "yyyy-MM-dd");

				ConferenceService.createConference.save(
					conferenceToSave,
					function(data) {
						$scope.forms.conf_form.$setPristine();
						$scope.currentConference = {};

						$modal.open({
							templateUrl: "/static/views/partials/dialogs/object_saved.html",
							controller: function($scope, $modalInstance) {
								$scope.closeDialog = function() {
									$modalInstance.dismiss("cancel");
								}
							}
						});

						$scope.$emit("newConferenceCreated", data);
					},
					function() {
						$modal.open({
							templateUrl: "/static/views/partials/dialogs/save_failed.html",
							controller: function($scope, $modalInstance) {
								$scope.closeDialog = function() {
									$modalInstance.dismiss("cancel");
								}
							}
						});
					}
				);
			};

			$scope.openStartDatePicker = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.datepickers.isStartDateOpen = !$scope.datepickers.isStartDateOpen;
			};

			$scope.openEndDatePicker = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.datepickers.isEndDateOpen = !$scope.datepickers.isEndDateOpen;
			};

			$scope.page = 1;
			$scope.venues = [];
			$scope.getVenues = function() {
				AddressService.address.get({ page: $scope.page++ }, function(data) {
					$scope.venues = $scope.venues.concat(data.results);
					if(data.next != null) {
						$scope.getVenues();
					} else {
						$scope.elaborateVenueIds();
					}
				});
			};
			$scope.getVenues();

			$scope.elaborateVenueIds = function() {
				$scope.venues_ids = $.map($scope.venues, function(val, i) {
					if($scope.conference.venue == val.address_id) {
						$scope.currentVenue = val;
					}

					return val.address_id;
				});
			};
		}
	]
);
