"use strict";

angular.module("ldcaApp").controller("DayController",
	[
		"$scope",
		"$log",
		"data",
		"TrackService",

		function($scope, $log, data, TrackService)
		{
			$scope.day = data[0];

			$scope.datepickers = {
				defaultOptions: {
					formatYear: "yyyy",
					startingDay: 0
				},
				isDateOpen: false
			};

			$scope.editing = false;

			$scope.openDatePicker = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.datepickers.isDateOpen = !$scope.datepickers.isDateOpen;
			};

			$scope.tracks = [];
			$.each($scope.day.tracks, function(k, v) {
				$scope.tracks.push(TrackService.track.get({id: v}));
			});

			$scope.editDay = function() {
				$scope.editing = true;
			};
			$scope.disableEditDay = function() {
				$scope.editing = false;
			};
		}
	]
);
