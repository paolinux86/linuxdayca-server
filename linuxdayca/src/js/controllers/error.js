"use strict";

angular.module("ldcaApp").controller("ErrorController",
	[
		"$scope",
		"$log",
		"$stateParams",

		function($scope, $log, $stateParams)
		{
			$scope.errorCode = $stateParams.error;
			$scope.from = $stateParams.from;
		}
	]
);
