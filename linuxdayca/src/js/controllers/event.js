"use strict";

angular.module("ldcaApp").controller("EventController",
	[
		"$scope",
		"$log",
		"data",
		"PersonService",
		"EventTypeService",

		function($scope, $log, data, PersonService, EventTypeService)
		{
			$scope.event = data[0];

			$scope.editing = false;

			$scope.people = [];
			$.each($scope.event.person, function(k, v) {
				$scope.people.push(PersonService.person.get({id: v}));
			});

			$scope.page = 1;
			$scope.event_types = [];
			$scope.getEventTypes = function() {
				EventTypeService.event_type.get({ page: $scope.page++ }, function(data) {
					$scope.event_types = $scope.event_types.concat(data.results);
					if(data.next != null) {
						$scope.getEventTypes();
					} else {
						$scope.elaborateeventTypeIds();
					}
				});
			};
			$scope.getEventTypes();

			$scope.elaborateeventTypeIds = function() {
				$scope.event_types_ids = $.map($scope.event_types, function(val, i) {
					if($scope.event.event_type == val.code) {
						$scope.currentEventType = val;
					}

					return val.code;
				});
			};

			$scope.editEvent = function() {
				$scope.editing = true;
			};
			$scope.disableEditEvent = function() {
				$scope.editing = false;
			};
		}
	]
);
