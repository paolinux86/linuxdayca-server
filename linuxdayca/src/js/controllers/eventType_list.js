"use strict";

angular.module("ldcaApp").controller("EventTypeListController",
	[
		"$scope",
		"$log",
		"$controller",
		"data",
		"EventTypeService",

		function($scope, $log, $controller, data, EventTypeService)
		{
			$controller("GenericListController", { $scope: $scope, $log: $log, data: data });
			$scope.onPagerChange = function() {
				EventTypeService.event_type.get({ page: $scope.currentPage }, $scope.setNewData);
			};
		}
	]
);
