"use strict";

angular.module("ldcaApp").controller("HomeController",
	[
		"$scope",
		"$log",
		"data",

		function($scope, $log, data)
		{
			$scope.previousConferences = data[0].results;
			$scope.previousConferencesTotal = data[0].count;
		}
	]
);
