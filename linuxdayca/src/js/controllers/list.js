"use strict";

angular.module("ldcaApp").controller("GenericListController",
	[
		"$scope",
		"$log",
		"data",

		function($scope, $log, data)
		{
			$scope.currentPage = 1;

			$scope.objects = data[0].results;
			$scope.totalItems = data[0].count;

			$scope.setNewData = function(data) {
				$scope.objects = data.results;
				$scope.totalItems = data.count;
			};
		}
	]
);
