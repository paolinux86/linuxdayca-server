"use strict";

angular.module("ldcaApp").controller("MainController",
	[
		"$scope",
		"$log",
		"UserService",
		"ConferenceService",

		function($scope, $log, UserService, ConferenceService)
		{
			//$scope.message = "Hello, World!!";
			$scope.user = UserService.user.get(
				function() {
					$scope.user.logged = true;
				},
				function() {
					$scope.user.logged = false;
				}
  			);

			$scope.conference = ConferenceService.currentConference.get();
		}
	]
);
