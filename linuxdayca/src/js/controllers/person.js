"use strict";

angular.module("ldcaApp").controller("PersonController",
	[
		"$scope",
		"$log",
		"data",

		function($scope, $log, data)
		{
			$scope.person = data[0];

			$scope.editing = false;

			$scope.editPerson = function() {
				$scope.editing = true;
			};
			$scope.disableEditPerson = function() {
				$scope.editing = false;
			};
		}
	]
);
