"use strict";

angular.module("ldcaApp").controller("PersonListController",
	[
		"$scope",
		"$log",
		"$controller",
		"data",
		"PersonService",

		function($scope, $log, $controller, data, PersonService)
		{
			$controller("GenericListController", { $scope: $scope, $log: $log, data: data });
			$scope.onPagerChange = function() {
				PersonService.person.get({ page: $scope.currentPage }, $scope.setNewData);
			};
		}
	]
);
