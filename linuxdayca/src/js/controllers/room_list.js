"use strict";

angular.module("ldcaApp").controller("RoomListController",
	[
		"$scope",
		"$log",
		"$controller",
		"data",
		"RoomService",

		function($scope, $log, $controller, data, RoomService)
		{
			$controller("GenericListController", { $scope: $scope, $log: $log, data: data });
			$scope.onPagerChange = function() {
				RoomService.room.get({ page: $scope.currentPage }, $scope.setNewData);
			};
		}
	]
);
