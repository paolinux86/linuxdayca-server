"use strict";

angular.module("ldcaApp").controller("TrackController",
	[
		"$scope",
		"$log",
		"data",
		"EventService",
		"RoomService",

		function($scope, $log, data, EventService, RoomService)
		{
			$scope.track = data[0];

			$scope.editing = false;

			$scope.events = [];
			$.each($scope.track.events, function(k, v) {
				$scope.events.push(EventService.event.get({id: v}));
			});

			$scope.page = 1;
			$scope.rooms = [];
			$scope.getRooms = function() {
				RoomService.room.get({ page: $scope.page++ }, function(data) {
					$scope.rooms = $scope.rooms.concat(data.results);
					if(data.next != null) {
						$scope.getRooms();
					} else {
						$scope.elaborateRoomIds();
					}
				});
			};
			$scope.getRooms();

			$scope.elaborateRoomIds = function() {
				$scope.rooms_ids = $.map($scope.rooms, function(val, i) {
					if($scope.track.room == val.name) {
						$scope.currentRoom = val;
					}

					return val.name;
				});
			};

			$scope.editTrack = function() {
				$scope.editing = true;
			};
			$scope.disableEditTrack = function() {
				$scope.editing = false;
			};
		}
	]
);
