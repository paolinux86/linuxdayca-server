"use strict";

angular.module("ldcaApp").controller("TrackCreateController",
	[
		"$scope",
		"$log",
		"$filter",
		"$modal",
		"TrackService",
		"RoomService",

		function($scope, $log, $filter, $modal, TrackService, RoomService)
		{
			$scope.forms = {};
			$scope.track = {};

			$scope.page = 1;
			$scope.rooms = [];
			$scope.getRooms = function() {
				RoomService.room.get({ page: $scope.page++ }, function(data) {
					$scope.rooms = $scope.rooms.concat(data.results);
					if(data.next != null) {
						$scope.getRooms();
					} else {
						$scope.elaborateRoomIds();
					}
				});
			};
			$scope.getRooms();

			$scope.elaborateRoomIds = function() {
				$scope.rooms_ids = $.map($scope.rooms, function(val, i) {
					if($scope.track.room == val.name) {
						$scope.currentRoom = val;
					}

					return val.name;
				});
			};

// 			$scope.saveTrack = function() {
// 				var conferenceToSave = $.extend({}, $scope.currentConference);
// 				var start_date = new Date($scope.currentConference.start_date);
// 				var end_date = new Date($scope.currentConference.end_date);
// 				conferenceToSave.start_date = $filter("date")(start_date, "yyyy-MM-dd");
// 				conferenceToSave.end_date = $filter("date")(end_date, "yyyy-MM-dd");
//
// 				ConferenceService.createConference.save(
// 					conferenceToSave,
// 					function(data) {
// 						$scope.forms.conf_form.$setPristine();
// 						$scope.currentConference = {};
//
// 						$modal.open({
// 							templateUrl: "/static/views/partials/dialogs/object_saved.html",
// 							controller: function($scope, $modalInstance) {
// 								$scope.closeDialog = function() {
// 									$modalInstance.dismiss("cancel");
// 								}
// 							}
// 						});
//
// 						$scope.$emit("newConferenceCreated", data);
// 					},
// 					function() {
// 						$modal.open({
// 							templateUrl: "/static/views/partials/dialogs/save_failed.html",
// 							controller: function($scope, $modalInstance) {
// 								$scope.closeDialog = function() {
// 									$modalInstance.dismiss("cancel");
// 								}
// 							}
// 						});
// 					}
// 				);
// 			};
		}
	]
);
