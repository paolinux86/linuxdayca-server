"use strict";

angular.module("ldcaApp").controller("WizardController",
	[
		"$scope",
		"$log",

		function($scope, $log)
		{
			$scope.noHeader = true;
			$scope.steps = [
				{ htmlToLoad: "/static/views/track_create.html", title: "Tracce" },
				{ htmlToLoad: "/static/views/conference_create.html", title: "Conferenza" },
				{ htmlToLoad: "/static/views/conference_create.html", title: "Autori" },
				{ htmlToLoad: "/static/views/conference_create.html", title: "Eventi" }
			];
			$scope.col_width = Math.floor(12 / $scope.steps.length);

			$scope.changePage = function(page) {
				$scope.currentStep = page;
				$scope.htmlToLoad = $scope.steps[$scope.currentStep].htmlToLoad;
			};

			$scope.changePage(0);

			$scope.$on("newConferenceCreated", function(event, conferenceSaved) {
				$log.info("newConferenceCreated called");
				$log.debug(conferenceSaved);

				$scope.changePage(1);
			});
		}
	]
);
