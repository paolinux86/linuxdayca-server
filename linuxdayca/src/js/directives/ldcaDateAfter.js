"use strict";

angular.module("ldca.date-module")
.directive("ldcaDateAfter", function() {
	var link = function($scope, $element, $attrs, ctrl) {
		var validate = function(viewValue) {
			var comparisonModel = $attrs.ldcaDateAfter;
			if(!viewValue || !comparisonModel || viewValue == NaN) {
				// It's valid because we have nothing to compare against
				ctrl.$setValidity("dateAfter", true);
				return viewValue;
			}

			var comparisonAsTimestamp = new Date(comparisonModel.substr(1, comparisonModel.length -2)).getTime();
			var viewValueAsTimestamp = viewValue.getTime();

			// It's valid if model is lower than the model we're comparing against
			ctrl.$setValidity("dateAfter", viewValue >= comparisonAsTimestamp);
			return viewValue;
		};

		ctrl.$parsers.unshift(validate);
		ctrl.$formatters.push(validate);

		$attrs.$observe("ldcaDateAfter", function(comparisonModel) {
			// Whenever the comparison model changes we'll re-validate
			return validate(ctrl.$viewValue);
		});
	};

	return {
		require: "ngModel",
		link: link
	};
});
