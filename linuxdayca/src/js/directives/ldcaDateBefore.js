"use strict";

angular.module("ldca.date-module")
.directive("ldcaDateBefore", function() {
	var link = function($scope, $element, $attrs, ctrl) {
		var validate = function(viewValue) {
			var comparisonModel = $attrs.ldcaDateBefore;
			if(!viewValue || !comparisonModel || viewValue == NaN) {
				// It's valid because we have nothing to compare against
				ctrl.$setValidity("dateBefore", true);
				return viewValue;
			}

			var comparisonAsTimestamp = new Date(comparisonModel.substr(1, comparisonModel.length -2)).getTime();
			var viewValueAsTimestamp = viewValue.getTime();

			// It's valid if model is lower than the model we're comparing against
			ctrl.$setValidity("dateBefore", viewValue <= comparisonAsTimestamp);
			return viewValue;
		};

		ctrl.$parsers.unshift(validate);
		ctrl.$formatters.push(validate);

		$attrs.$observe("ldcaDateBefore", function(comparisonModel) {
			// Whenever the comparison model changes we'll re-validate
			return validate(ctrl.$viewValue);
		});
	};

	return {
		require: "ngModel",
		link: link
	};
});
