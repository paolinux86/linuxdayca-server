var services = angular.module("ldcaApp.services", ["ngResource"]);

services.factory("UserService",
	[
		"$resource",

		function($resource)
		{
			return {
				user: $resource("/api/user")
			};
		}
	]
);

services.factory("ConferenceService",
	[
		"$resource",

		function($resource)
		{
			return {
				currentConference: $resource("/api/conference/current"),
				conference: $resource("/api/conference/:id"),
				statistics: $resource("/api/conference/:id/stats"),
				createConference: $resource("/api/conference/new"),
				previousConferences: $resource("/api/conference/previous")
			};
		}
	]
);

services.factory("AddressService",
	[
		"$resource",

		function($resource)
		{
			return {
				address: $resource("/api/address/:id")
			};
		}
	]
);

services.factory("DayService",
	[
		"$resource",

		function($resource)
		{
			return {
				day: $resource("/api/day/:id")
			};
		}
	]
);

services.factory("TrackService",
	[
		"$resource",

		function($resource)
		{
			return {
				track: $resource("/api/track/:id")
			};
		}
	]
);

services.factory("EventService",
	[
		"$resource",

		function($resource)
		{
			return {
				event: $resource("/api/event/:id")
			};
		}
	]
);

services.factory("RoomService",
	[
		"$resource",

		function($resource)
		{
			return {
				room: $resource("/api/room/:id/?page=:page")
			};
		}
	]
);

services.factory("PersonService",
	[
		"$resource",

		function($resource)
		{
			return {
				person: $resource("/api/person/:id/?page=:page")
			};
		}
	]
);

services.factory("EventTypeService",
	[
		"$resource",

		function($resource)
		{
			return {
				event_type: $resource("/api/event_type/:id/?page=:page")
			};
		}
	]
);
