from django.conf.urls import patterns, include, url

from django.views.generic import TemplateView
from linuxdayca.api.views import CustomTemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns("linuxdayca.api.views",
	url(r"^login/$", "login_view"),
	url(r"^logout/$", "logout_view"),
)

urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r"^docs/", include('rest_framework_swagger.urls')),
    url(r'^api/', include('linuxdayca.api.urls')),
)

urlpatterns += patterns("",
	url(r'^$', TemplateView.as_view(template_name='index.html')),
	url(r'^(?P<template_name>[a-z_]+).html$', CustomTemplateView.as_view()),
)
